package kevin;

public class KevinClass {
	public static void main(String[] args) {
		String input = "my name is kevin";

		String[] words = input.split("\\s");
		StringBuilder output = new StringBuilder();

		for (int i = words.length-1; i >= 0; i--) {
			if (i == 0) {
				output.append(words[i]);
			} else {
				output.append(words[i]).append(" ");
			}
		}

		System.out.println(output);
	}
}
