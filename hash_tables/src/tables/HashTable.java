package tables;

/**
 * Hash Table using linear probing
 *
 * @author Ryan Hendrickson
 * @version 1.0
 */
public class HashTable<T> {
	private static final double MAX_LOAD_FACTOR = 0.6;
	private static final int DEFAULT_TABLE_SIZE = 10;
	// fields
	private Element<T>[] table;
	private int size;

	/**
	 * create a table with an initial (default) size
	 */
	public HashTable() {
		table = new Element[DEFAULT_TABLE_SIZE];
	}

	/**
	 * create a table with a user provided size
	 *
	 * @param size = starting size (must be positive)
	 */
	public HashTable(int size) {
		table = new Element[size];
	}

	/**
	 * adds an element to the table if the element is not already present
	 *
	 * @param element - element to add to table
	 * @return true if element is successfully added, otherwise false
	 */
	public boolean add(T element) {
		// if we exceed load factor, resize
		double loadFactor = (double) size/table.length;
		if (loadFactor >= MAX_LOAD_FACTOR) {
			resize();
		}

		// get hashcode of element
		int hashCode = Math.abs(element.hashCode());
		int index = hashCode % table.length;

		// find a spot for element if not already present
		while (table[index] != null) {

			// if the element is a duplicate
			if (table[index].data.equals(element)) {
				return false;
			}

			// move to next index
			index = (index + 1) % table.length;
		}

		// assign element
		table[index] = new Element<>(element, false);
		size++;

		return true; // I found a spot for the element
	}

	private void resize() {
		// save the old table and reset size
		Element<T>[] oldTable = table;
		size = 0;

		// make a new table, and double the size
		table = new Element[oldTable.length * 2];

		// loop over elements in the old table and if not removed, rehash them
		for (int i = 0; i < oldTable.length; i++) {
			if (oldTable[i] != null && !oldTable[i].previouslyRemoved) {
				add(oldTable[i].data);
			}
		}
	}

	/**
	 * checks to see if the table contains said element
	 *
	 * @param element - element to search for
	 * @return true if element is in table, otherwise false
	 */
	public boolean contains(T element) {
		// 1: check to see if element is null
		if (element == null) {
			return false;
		}

		// get the hashcode + index
		int hashCode = Math.abs(element.hashCode());
		int index = hashCode % table.length;

		// 2: start looping at the provided index
		while (table[index] != null) {

			// see if the element equals anything in the hash table that hasn't been removed
			if (table[index].data.equals(element) && !table[index].previouslyRemoved) {
				return true;
			}

			// move to next index
			index = (index + 1) % table.length;
		}
		return false;
	}

	/**
	 * removes an element in the table, if found
	 *
	 * @param element - element to remove
	 * @return true if element is found and removed, otherwise false
	 */
	public boolean remove(T element) {
		// check to see if element is null
		if (element == null) {
			return false;
		}

		// get the hashcode + index
		int hashCode = Math.abs(element.hashCode());
		int index = hashCode % table.length;

		// 2: start looping at the provided index
		while (table[index] != null) {

			// if we find element, set it to removed (previouslyRemoved)
			if (table[index].data.equals(element) && !table[index].previouslyRemoved) {
				table[index].previouslyRemoved = true;
				size--;
				return true;
			}

			// move to next index
			index = (index + 1) % table.length;
		}

		return false;
	}

	private static class Element<T> {
		private T data;
		private boolean previouslyRemoved;

		public Element(T data, boolean previouslyRemoved) {
			this.data = data;
			this.previouslyRemoved = previouslyRemoved;
		}

		@Override
		public String toString() {
			return data + (previouslyRemoved ? ", removed!" : "");
		}
	}
}
