package test;

import trees.BinarySearchTree;

public class TestTree {

	public static void main(String[] args) {
		// add a tree with elements to add
		BinarySearchTree<Integer> numTree = new BinarySearchTree<>();
		int[] elemsToAdd = {25, 11, 77, 69, 3, 50, 39, 55, 99, 42};

		// add them
		for (int elem : elemsToAdd) {
			numTree.add(elem);
		}
		System.out.println("Size of tree: " + numTree.size());
		System.out.println("Does numTree contain 69? " + numTree.contains(69));
		System.out.println("Does numTree contain 69? " + numTree.contains(420));
		System.out.println("Lowest value: " + numTree.min());
		System.out.println("Highest value: " + numTree.max());

		System.out.println("Removing 3...");
		numTree.remove(3);
		System.out.println("New size of tree: " + numTree.size());
		System.out.println("New lowest value: " + numTree.min());

	}

}
