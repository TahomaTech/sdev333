package trees;

import java.util.*;

public class BinarySearchTree<T extends Comparable<T>> implements ISearchTree<T> {
	// field
	private Node root;
	private int size;
	private int modCount; // for iterator

	@Override
	public boolean add(T element) {
		// is the tree empty?
		if (root == null) {
			root = new Node(element);
			size++;
			modCount++; // a change was made
			return true;
		} else { // move down tree and place the new element
			// start the search at the root | this way allows us to reassign references on our way back up the recursion
			int savedSize = size;
			root = add(root, element);
			if (savedSize != size) {
				modCount++;
				return true;
			}
			return false;
		}
	}

	// recursive method to add new Node object
	private Node add(Node current, T element) {
		// if we encounter a valid spot for the element
		if (current == null) {
			size++;
			modCount++;
			return new Node(element);
		}

		// traverse down tree
		int comparison = current.data.compareTo(element);
		if (comparison < 0) { // data in node is smaller than data being added, so we go RIGHT
			current.right = add(current.right, element);
		} else if (comparison > 0) { // data in node is bigger than data being added, so we go LEFT
			current.left = add(current.left, element);
		}
		return current;
	}

	@Override
	public boolean contains(T element) {
		return contains(root, element);
	}

	private boolean contains (Node current, T element) {
		// didn't find it
		if (current == null) {
			return false;
		}

		// search for the element
		int comparison = current.data.compareTo(element);
		if (comparison == 0) { // found it
			return true;
		} else if (comparison < 0) { // go right
			return contains(current.right, element);
		} else { // go left
			return contains(current.left, element);
		}
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return root == null && size == 0;
	}

	@Override
	public void clear() {
		root = null;
		size = 0;
		modCount = 0;
	}

	@Override
	public boolean remove(T element) {
		// is tree empty?
		if (root == null) {
			return false;
		}
		// start search for element to remove
		int savedSize = size;
		root = remove(root, element);
		if (savedSize != size) {
			modCount++;
			return true;
		}
		return false;
	}

	private Node remove (Node current, T element) {
		// if we reach a leaf
		if (current == null) {
			return null; // not found
		}
		// search for element
		int comparison = current.data.compareTo(element);
		if (comparison == 0) { // we found it
			if (current.left == null && current.right == null) { // no children
				size--;
				return null;
			} else if (current.left != null && current.right != null) { // two children
				// find maximum in the left subtree
				T max = max(current.left);

				// replace the data in the current node
				current.data = max;

				// remove the node containing the maximum in the left subtree (We need to pass in current.left so we don't
				// delete our replaced data from the last step
				current.left = remove(current.left, max);
			} else if (current.left != null) { // left child
				size--;
				return current.left;
			} else { // right child
				size--;
				return current.right;
			}
		} else if (comparison < 0) { // go right
			current.right = remove(current.right, element);
		} else { // comparison > 0 | go left
			current.left = remove(current.left, element);
		}
		// remove the element
		return current;
	}

	@Override
	public T min() {
		// if no minimum
		if (root == null) {
			return null;
		} else {
			return min(root);
		}
	}

	// private helped method for our public min | MOST SEEN TECHNIQUE WITH RECURSION
	private T min(Node current) {
		if (current.left == null) {
			return current.data;
		} else {
			// if current is not the smallest, move left
			return min(current.left);
		}
	}

	@Override
	public T max() {
		// if no maximum
		if (root == null) {
			return null;
		} else {
			return max(root);
		}
	}

	// private helped method for our public max | MOST SEEN TECHNIQUE WITH RECURSION
	private T max(Node current) {
		if (current.right == null) {
			return current.data;
		} else {
			// if current is not the highest, move right
			return max(current.right);
		}
	}

	@Override
	public T getRoot() {
		return root.data;
	}

	@Override
	public int treeHeight() {
		return 0;
	}

	@Override
	public boolean depth() {
		return false;
	}

	// traversal order: LNR
	public List<T> inOrder() {
		List<T> traversal = new ArrayList<>();
		inOrder(root, traversal);
		return traversal;
	}

	private void inOrder(Node current, List<T> traversal) {
		if (current == null) {
			return; // exit
		}

		// now we do left, node, right
		inOrder(current.left, traversal);
		traversal.add(current.data);
		inOrder(current.right, traversal);
	}

	// traversal order: NLR
	public List<T> preOrder() {
		List<T> traversal = new ArrayList<>();
		preOrder(root, traversal);
		return traversal;
	}

	private void preOrder(Node current, List<T> traversal) {
		if (current == null) {
			return; // exit
		}

		// now we do node, left, right
		traversal.add(current.data);
		preOrder(current.left, traversal);
		preOrder(current.right, traversal);
	}

	// traversal order: LRN
	public List<T> postOrder() {
		List<T> traversal = new ArrayList<>();
		postOrder(root, traversal);
		return traversal;
	}

	private void postOrder(Node current, List<T> traversal) {
		if (current == null) {
			return; // exit
		}

		// now we do left, right, node
		postOrder(current.left, traversal);
		postOrder(current.right, traversal);
		traversal.add(current.data);
	}

	@Override
	public Iterator<T> iterator() {
		// List has their own iterator, use theirs
		return inOrder().iterator();
		// return new BSTIterator; is the "naive way"
	}

	private class BSTIterator implements Iterator<T> {
		// simulates the Java call stack
		private Stack<Node> stack;

		public BSTIterator() {
			this.stack = new Stack<>();

			// move to the first node to be returned
			Node current = root;
			while (current != null) {
				stack.push(current);
				current = current.left;
			}
		}

		@Override
		public boolean hasNext() {
			// if there's a node on the stack, we're doing good
			return !stack.isEmpty();
		}

		@Override
		public T next() {
			// save top element of stack to return
			Node current = stack.pop();
			T result = current.data;

			// add elements to the stack, starting at right child
			current = current.right;
			while (current != null) {
				stack.push(current);
				current = current.left;
			}

			return result;
		}
	}

	private class NaiveIterator implements Iterator<T> {
		// index of the next element to return from the iterator!
		private List<T> elements;
		private int currentIndex = 0;
		private int savedModeCount;

		public NaiveIterator() {
			elements = inOrder();
			savedModeCount = modCount;
		}

		@Override
		public boolean hasNext() {
			concurrentChangesCheck();
			return currentIndex < elements.size();
		}

		@Override
		public T next() {
			concurrentChangesCheck();
			return elements.get(currentIndex++);
		}

		private void concurrentChangesCheck() {
			if (savedModeCount != modCount) {
				throw new ConcurrentModificationException("Cannot alter the tree while iterating");
			}
		}

		@Override
		public String toString() {
			return "BSTIterator{" +
					"elements=" + elements +
					", currentIndex=" + currentIndex +
					", savedModeCount=" + savedModeCount +
					'}';
		}
	}

	public class Node {
		private T data;
		private Node left;
		private Node right;

		public Node(T data, Node left, Node right) {
			this.data = data;
			this.left = left;
			this.right = right;
		}

		public Node(T data) {
			this.data = data;
		}

		@Override
		public String toString() {
			return "Node{" +
					"data=" + data +
					", left=" + left +
					", right=" + right +
					'}';
		}
	}
}
