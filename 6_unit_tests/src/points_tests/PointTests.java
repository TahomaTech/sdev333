package points_tests;

import org.junit.Assert;
import org.junit.Test;
import points.Point2D;

public class PointTests {

	@Test
	public void parameterizedConstructorTest() {
		Point2D point = new Point2D(10, 3);

		Assert.assertEquals("Parameterized constructor is not returning x value that was passed in.",
				10, point.getX(), 0.0); // 0.0 is our delta value (exact match)
		Assert.assertEquals("Parameterized constructor is not returning y value that was passed in.",
				3, point.getY(), 0.0);
	}

	@Test
	public void originTest() {
		Point2D origin = new Point2D();

		Assert.assertTrue("Default constructor not setting X and Y points at origin", origin.isAtOrigin());
	}

	@Test
	public void notAtOriginTest() {
		Point2D origin = new Point2D(10, 3);

		Assert.assertFalse("Parameterized constructor isAtOrigin() is not returning false", origin.isAtOrigin());
	}

	@Test
	public void distanceTest() {
		Point2D p1 = new Point2D();
		Point2D p2 = new Point2D(3, 4);

		Assert.assertEquals("Distance of the hypotenuse 5-4-3 triangle is incorrect",
				5, p1.distance(p2), 0.0);
	}

	@Test
	public void samePointDistanceTest() {
		Point2D p1 = new Point2D();
		Point2D p2 = new Point2D();

		Assert.assertEquals("Distance of two of the same points is not zero",
				0.0, p1.distance(p2), 0.0);
	}

}
