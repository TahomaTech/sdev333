package tests;

import data_structures.Bag;

import java.util.Iterator;
import java.util.Random;

/**
 * used to test our Bag class
 * @author Ryan Hendrickson
 * @version 1
 */
public class BagOfHolding {
	/**
	 * @param args - we all know
	 */
	public static void main(String[] args) {
		final int BAG_SIZE = 10;

		Bag<Integer> myBag = new Bag<>(BAG_SIZE);

		Random random = new Random();
		for (int i = 0; i < BAG_SIZE; i++) {
			myBag.add(random.nextInt(10));
		}

		System.out.println(myBag);

		System.out.println("Is 10 in the bag? " + myBag.contains(10));
		System.out.println("How big is our bag? " + myBag.size());
		System.out.println("Is our bag empty? " + myBag.isEmpty());

		System.out.println("Emptying our bag...");
		myBag.clear();
		System.out.println("Is the bag empty? " + myBag.isEmpty());

		System.out.println("Adding new items...");
		myBag.add(2);
		myBag.add(4);
		myBag.add(6);
		myBag.add(8);
		myBag.add(10);
		System.out.println("Seeing if remove works...");
		myBag.remove(2);
		myBag.remove(6);
		myBag.remove(10);

		// using an Iterator directly
		Iterator<Integer> numIt = myBag.iterator();
		while (numIt.hasNext()) {
			System.out.println(numIt.next());
		}

		// using an Iterator indirectly
		for (int num : myBag) {
			System.out.println(num);
		}
	}
}
