package test;

import structures.ReversibleStackLL;

/**
 * Driver drives the ReversibleStackLL class
 * @author Ryan H.
 * @version 1
 */
public class Driver {

	/**
	 * entry point to program
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		ReversibleStackLL<Integer> stack = new ReversibleStackLL<>();

		// test add ()
		stack.add(1);
		stack.add(2);
		stack.add(3);
		stack.add(4);
		stack.add(5);
		System.out.println("Initial populating of stack: " + stack.toString());

		// test remove()
		stack.add(6);
		System.out.println("Added a new number: " + stack.toString());
		// stack.remove(4); commented out because it fails and stops the program (as expected)
		stack.remove();
		System.out.println("Deleted the last entered number: " + stack.toString());

		// test contains()
		System.out.println("Does stack contain a 2? " + stack.contains(2)); // true
		System.out.println("Does stack contain a 6? " + stack.contains(6)); // false

		// test size()
		System.out.println("Size of the stack is: " + stack.size());

		// test isEmpty()
		System.out.println("Is our stack empty? " + stack.isEmpty());

		// test clear()
		stack.clear();
		System.out.println("Now we clear the whole stack: " + stack.toString());
		System.out.println("Size of stack: " + stack.size());
		System.out.println("Is our stack empty? " + stack.isEmpty());

		// repopulate list
		stack.add(1);
		stack.add(2);
		stack.add(3);
		stack.add(4);
		stack.add(5);
		System.out.println("Repopulated stack: " + stack.toString());

		// test reverse()
		stack.reverse();
		System.out.println("Reversed stack: " + stack.toString());
	}
}
