package structures;

/**
 * Node class that is double linked that accepts generic data types
 * @author Ryan H.
 * @version 1.0
 * @param <T> is our generic type identifier
 */
public class Node<T> {
	// fields
	private T data;
	private Node<T> next;
	private Node<T> prev;

	/**
	 * constructor for user to enter data, previous Node, and next Node
	 * @param data - data to be stored
	 * @param prev - previous node
	 * @param next - next node
	 */
	public Node(T data, Node<T> prev, Node<T> next) {
		this.data = data;
		this.prev = prev;
		this.next = next;
	}

	/**
	 * constructor for user to enter data and previous Node
	 * @param data - data to be stored
	 * @param prev - previous node
	 */
	public Node(T data, Node<T> prev) {
		this(data, prev, null);
	}

	/**
	 * constructor for user to only enter the data
	 * @param data - data to be stored
	 */
	public Node(T data) {
		this(data, null, null);
	}

	/**
	 * create a Node with null for everything
	 */
	public Node() {
		this(null, null, null);
	}

	/**
	 * getter for data
	 * @return data
	 */
	public T getData() {
		return data;
	}

	/**
	 * setter for data
	 * @param data - passed in data to override current data
	 */
	public void setData(T data) {
		this.data = data;
	}

	/**
	 * getter for next
	 * @return next
	 */
	public Node<T> getNext() {
		return next;
	}

	/**
	 * setter for next
	 * @param next - passed in next to override current next
	 */
	public void setNext(Node<T> next) {
		this.next = next;
	}

	/**
	 * getter for prev
	 * @return prev
	 */
	public Node<T> getPrev() {
		return prev;
	}

	/**
	 * setter for prev
	 * @param prev - passed in prev to override current prev
	 */
	public void setPrev(Node<T> prev) {
		this.prev = prev;
	}

	@Override
	public String toString() {
		return "Node{" +
				"data=" + data +
				", next=" + next +
				", prev=" + prev +
				'}';
	}
}
