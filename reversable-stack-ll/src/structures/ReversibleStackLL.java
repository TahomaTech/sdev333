package structures;

import adts.IReversableStack;

import java.util.*;

/**
 * ReversibleStackLL is a data structure class using linked lists to connect everything
 * @author Ryan H.
 * @version 1.0
 * @param <T>
 */
public class ReversibleStackLL<T extends Comparable<T>> implements IReversableStack<T>, Iterable<T> {
	// fields
	private Node<T> head; // first object put in
	private Node<T> tail; // last object put in
	private int size;
	private int modCount = 0;

	/**
	 * Adds an element to the collection.
	 * @param element - the element to add
	 */
	@Override
	public void add(T element) {
		if (head == null || head.getData() == null) {
			head = new Node<>(element);
			tail = head;
		} else {
			Node<T> current = head;
			while(current.getNext() != null) {
				current = current.getNext();
			}
			current.setNext(new Node<>(element, current));
			tail = current.getNext();
		}
		size++;
		modCount++;
	}

	/**
	 * This method is not supported in the structure.
	 * @param element - the element to remove
	 * @throws UnsupportedOperationException when the method is called
	 */
	@Override
	public void remove(T element) {
		throw new UnsupportedOperationException("This operation is not supported with stacks");
	}

	/**
	 * Removes, and returns, the top element of the stack.
	 * @return data - the top element of the stack, we return a variable though, so we can assign everything on the last
	 * Node to null so our garbage collector can gobble it up like Megan Thee Stallion
	 * @throws java.util.NoSuchElementException when the stack is empty
	 */
	@Override
	public T remove() {
		if (head == null) {
			throw new NoSuchElementException("Stack is empty");
		}  else if (size == 1) {
			T data = head.getData();
			head = null;
			tail = null;
			size--;
			modCount++;
			return data;
		} else {
			Node<T> current = tail;
			tail = tail.getPrev();
			tail.setNext(null);
			T data = current.getData();
			current.setPrev(null);
			current.setNext(null);
			current.setData(null);
			size--;
			modCount++;
			return data;
		}
	}

	/**
	 * Returns true if the element is located in the collection, otherwise false
	 * @param element - the element to search for
	 * @return true if the element is found, otherwise false
	 */
	@Override
	public boolean contains(T element) {
		if (!isEmpty()) {
			Node<T> current = head;
			do {
				if (current.getData().equals(element)) {
					return true;
				} else {
					current = current.getNext();
				}
			} while (current != null);
		}
		return false;
	}

	/**
	 * Returns the number of elements in the collection.
	 * @return size - the element count
	 */
	@Override
	public int size() {
		return size;
	}

	/**
	 * Returns true if the collection is empty, or otherwise false
	 * @return true if the collection is empty, or otherwise false
	 */
	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	/**
	 * Removes all elements from the collection. We iterate from the end to the front so that we can make everything null
	 * and prevent any reference loitering
	 * @throws java.util.NoSuchElementException when the stack is empty
	 */
	@Override
	public void clear() {
		head = null;
		tail = null;
		size = 0;
		modCount++;
	}

	/**
	 * Reverses the order of all elements in the stack.
	 * For example: (bottom) a, b, c, d (top) becomes
	 *              (bottom) d, c, b, a (top) after calling reverse()
	 */
	@Override
	public void reverse() {
		Stack<T> stack = new Stack<>();
		Node<T> current = head;
		while (current != null) {
			stack.push(current.getData());
			current = current.getNext();
		}
		current = head;
		while (current != null) {
			current.setData(stack.pop());
			current = current.getNext();
		}
	}

	/**
	 * public facing iterator for the ReversibleStackLL
	 * @return a new iterator that's been passed the modCount field so we can check for concurrent changes
	 */
	@Override
	public Iterator<T> iterator() {
		return new RSLLIterator<>(modCount);
	}

	/**
	 * private inner class that is our iterator
	 * @param <T>
	 */
	private class RSLLIterator<T extends Comparable<T>> implements Iterator<T> {
		// fields
		private int savedModCount;
		private Node<T> current;

		/**
		 * public facing iterator constructor that saves the modCount that's passed in, and creates a new Node current
		 * @param savedModCount - modCount's value when iterator was made
		 */
		public RSLLIterator(int savedModCount) {
			this.savedModCount = savedModCount;
			current = (Node<T>) tail;
		}

		/**
		 * checks to see if there is another element in the stack
		 * @return false if the current Node is null, otherwise true
		 */
		@Override
		public boolean hasNext() {
			checkConcurrentChanges();
			return current != null;
		}

		/**
		 * grabs the data from our Node and then moves current to the next Node
		 * @return the data of the Node
		 */
		@Override
		public T next() {
			checkConcurrentChanges();
			T data = current.getData();
			current = current.getPrev();
			return data;
		}

		/**
		 * helper method that checks to make sure the user isn't doing concurrent changes
		 * @throws ConcurrentModificationException if the savedModCount and modCount don't match.
		 */
		private void checkConcurrentChanges() {
			// if modified count has deviated
			if (modCount != savedModCount) {
				throw new ConcurrentModificationException("You cannot change the structure while iterating...");
			}
		}

		@Override
		public String toString() {
			return "RSLLIterator{" +
					"savedModCount=" + savedModCount +
					'}';
		}
	}

	@Override
	public String toString() {
		if(head == null || head.getData() == null) {
			return "[]";
		} else {
			StringBuilder result = new StringBuilder("bottom [" + head.getData());
			Node<T> current = head.getNext();
			while(current != null) {
				result.append(", ").append(current.getData());
				current = current.getNext();
			}
			result.append("] top");
			return result.toString();
		}

	}


}
