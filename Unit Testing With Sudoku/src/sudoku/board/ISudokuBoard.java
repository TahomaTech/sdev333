package sudoku.board;

import sudoku.exceptions.InvalidBoardPositionException;

/**
 * Represents the features of a Sudoku board.
 *
 * DO NOT ALTER THIS FILE!
 *
 * @author Josh Archer
 * @version 1.0
 */
public interface ISudokuBoard
{
    //constants
    int BOARD_SIZE = 9;
    int TOTAL_CELLS = BOARD_SIZE * BOARD_SIZE;
    int REGION_SIZE = 3;

    /**
     * Resets the board to an empty board and then fills the given number
     * of spots in the sudoku board with pre-existing elements for a player
     * to begin the match with. These present restrictions which can
     * increase the difficulty of the game.
     *
     * Each time this method is called the underlying solution for the board
     * is changed.
     *
     * @param spots the number of spots to fill
     * @throws IllegalArgumentException thrown if the number of spots
     * is not in the range [0,81].
     */
    void populateBoard(int spots);

    /**
     * Changes the value on the Sudoku board at the given
     * row & column.
     *
     * @param choice a number in the range [1,9]
     * @param row a row index in the range [0,8]
     * @param column a column index in the range [0,8]
     * @throws IllegalArgumentException thrown if the choice is not in
     * the range [1,9]
     * @throws InvalidBoardPositionException thrown if the row or column is not
     * in the range [0,8]
     */
    void makeChoice(int choice, int row, int column);

    /**
     * Returns a copy of the internal sudoku board.
     *
     * @return a 9x9 grid with cells that have player
     * choices from 1-9 or are empty, which is represented
     * by 0.
     */
    int[][] getBoard();

    /**
     * Reports whether the given row contains a contradiction or not.
     * This does not check whether the row has values in all locations,
     * but instead whether there are duplicate elements or not.
     *
     * @param row a number in the range [0,8]
     * @throws InvalidBoardPositionException thrown if the row is not
     * in the range [0,8]
     * @return returns false if there is a contradiction with the numbers
     * in the row, otherwise returns true
     */
    boolean isRowValid(int row);

    /**
     * Reports whether the given column contains a contradiction or not.
     * This does not check whether the column has values in all locations,
     * but instead whether there are duplicate elements or not.
     *
     * @param column a number in the range [0,8]
     * @throws InvalidBoardPositionException thrown if the column is not
     * in the range [0,8]
     * @return returns false if there is a contradiction with the numbers
     * in the column, otherwise returns true
     */
    boolean isColumnValid(int column);

    /**
     * Reports whether the given region contains a contradiction or not.
     * This does not check whether the region has values in all locations,
     * but instead whether there are duplicate elements or not.
     *
     * A region is one of the 9 groups of 9 cells that make up the sudoku
     * board.
     *
     * @param region a number in the range [0,8]
     * @throws InvalidBoardPositionException thrown if the region is not
     * in the range [0,8]
     * @return returns false if there is a contradiction with the numbers
     * in the region, otherwise returns true
     */
    boolean isRegionValid(int region);

    /**
     * Reports whether the user has made a choice at every
     * available position on the board.
     *
     * @return returns true if the board has a choice at each
     * position, or false otherwise
     */
    boolean isBoardFull();

    /**
     * Reports whether the user has solved the Sudoku game.
     *
     * @return returns true if there are no contradictions on the
     * board and all locations have a number in the range [1,9]
     */
    boolean isSolved();
}
