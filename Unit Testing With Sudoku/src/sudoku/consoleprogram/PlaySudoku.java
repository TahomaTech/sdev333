package sudoku.consoleprogram;

import sudoku.board.ISudokuBoard;
import sudoku.board.Sudoku;

/**
 * A small console program that lets the player solve
 * a Sudoku puzzle.
 *
 * DO NOT ALTER THIS FILE!
 *
 * @author Josh Archer
 * @version 1.0
 */
public class PlaySudoku
{
    private static ISudokuBoard board;

    /**
     * Entry point for the Sudoku1 console program.
     * @param args command-line arguments (not used)
     */
    public static void main(String[] args)
    {
        printWelcome();
        prepareMatch();
        solveBoard();
    }

    private static void printWelcome()
    {
        System.out.println("Welcome to the Sudoku1 Console Game!");
        System.out.println("***********************************");
        System.out.println();
    }

    private static void prepareMatch()
    {
        System.out.println("Generating a new Sudoku1 board...");
        board = new Sudoku();
        System.out.println("How many cells should I reveal [0,81]? ");
        board.populateBoard(Console.getInt());
    }

    private static void solveBoard()
    {
        while (!board.isSolved())
        {
            System.out.println(board.toString());
            System.out.println("Board incomplete");

            int row = Console.getInt("Row?");
            int column = Console.getInt("Column?");
            int choice = Console.getInt("Number?");

            board.makeChoice(choice, row, column);
        }

        System.out.println(board.toString());
        System.out.println("Congratulations, you win!");
    }
}
