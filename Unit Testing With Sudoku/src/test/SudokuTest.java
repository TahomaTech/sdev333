package test;

import flawedsolutions.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import sudoku.board.ISudokuBoard;
import sudoku.board.Sudoku;
import sudoku.exceptions.InvalidBoardPositionException;

import java.util.Arrays;

/**
 * Provides a series of tests that verifies the functionality
 * of the Sudoku class.
 * @author Ryan H.
 * @version 1.0
 */
public class SudokuTest {
	// fields
	private ISudokuBoard sudoku;

	private ISudokuBoard getBoard() {
		//working...
		return new Sudoku();
	}

	/**
	 * This method should assign the sudoku field
	 * with a new Sudoku object, so that each @Test
	 * method below has a new game board to work with.
	 */
	@Before
	public void setup() {
		sudoku = getBoard();
	}

	/**
	 * this method will check if the constructor is properly creating an empty board full of 0's
	 */
	@Test
	public void constructorTest() {
		int count = 0;
		for (int i = 0; i < sudoku.BOARD_SIZE; i++) {
			for (int j = 0; j < sudoku.BOARD_SIZE; j++) {
				if (sudoku.getBoard()[i][j] == 0) {
					count++;
				}
			}
		}
		Assert.assertEquals("Empty sudoku board should have 81 0's but doesn't.", 81, count);
	}

	/**
	 * this method will populate the board with TEXT_NUM elements then will make sure each region, column, and row are
	 * valid, and will finally make sure that TEST_NUM elements were actually added to the sudoku board
	 */
	@Test
	public void populateBoardTest() {
		sudoku.populateBoard(43);

		// counter to see if 43 items populate the board
		int count = 0;
		// acts as row checker for count
		for (int i = 0; i < sudoku.BOARD_SIZE; i++) {
			// acts as column checker for count
			for (int j = 0; j < sudoku.BOARD_SIZE; j++) {
				if (sudoku.getBoard()[i][j] != 0) {
					count++;
				}
			}
		}
		// check that our TEST_NUM == count, verifying the board populated the right amount of elements
		Assert.assertEquals("After calling .populateBoard(43) there wasn't 43 numbers in the board.", 43, count);

		sudoku.populateBoard(81);
		for (int i = 0; i < sudoku.BOARD_SIZE; i++) {
			// check to see that each region, row, and column are valid, so we know it pulled from a valid solution
			Assert.assertEquals("Region: " + i  + " is not valid which means it isn't reading in valid boards properly.",
					true, sudoku.isRegionValid(i));
			Assert.assertEquals("Row: " + i + " is not valid which means it isn't reading in valid boards properly.",
					true, sudoku.isRowValid(i));
			Assert.assertEquals("Column: " + i + " is not valid which means it isn't reading in valid boards properly.",
					true, sudoku.isColumnValid(i));
		}
	}

	/**
	 * this method will call populateBoard(TEST_NUM_BAD) to make sure IllegalArgumentException gets thrown properly
	 */
	@Test (expected = IllegalArgumentException.class)
	public void populateBoardTestIAE() {
		sudoku.populateBoard(100);
	}

	/**
	 * this method makes sure we can makeChoices and they get added to the board properly
	 */
	@Test
	public void makeChoiceTest() {
		sudoku.makeChoice(3, 3, 3);
		Assert.assertEquals("Element at row: 3, column: 3 didn't match input of 3.", 3, sudoku.getBoard()[3][3]);

		sudoku.makeChoice(5, 3, 3);
		Assert.assertEquals("Element at row: 3, column: 3 didn't match input of 5.", 5, sudoku.getBoard()[3][3]);

		sudoku.makeChoice(7, 3, 3);
		Assert.assertEquals("Element at row: 3, column: 3 didn't match input of 7.", 7, sudoku.getBoard()[3][3]);
	}

	/**
	 * this method will deliberately throw an IllegalArgumentException because we're trying to submit data outside 1-9
	 */
	@Test (expected = IllegalArgumentException.class)
	public void makeChoiceTestIAE() {
		sudoku.makeChoice(34, 3, 3);
		sudoku.makeChoice(12, 3, 3);
		sudoku.makeChoice(25, 3, 3);
		sudoku.makeChoice(654, 3, 3);
		sudoku.makeChoice(54, 3, 3);
		sudoku.makeChoice(93, 3, 3);
	}

	/**
	 * this method will deliberately throw an InvalidBoardPositionException because we're trying submit rows/columns
	 * outside 0-8
	 */
	@Test (expected = InvalidBoardPositionException.class)
	public void makeChoiceTestIBPE() {
		sudoku.makeChoice(3, 33, 3);
		sudoku.makeChoice(3, 84, 3);
		sudoku.makeChoice(3, 28, 3);
		sudoku.makeChoice(3, 3, 943);
		sudoku.makeChoice(3, 3, 49);
		sudoku.makeChoice(3, 3, 82);
	}

	/**
	 * this method creates a copy of a blank board, then uses Arrays.deepEquals to see if they're a deep copy, then
	 * does arr == sudoku.getBoard() to see if they're a shallow copy
	 */
	@Test
	public void getBoardTest() {
		int[][] arr = sudoku.getBoard();

		// verify we have a deep copy
		Assert.assertEquals(".getBoard() is not returning a copy, and is instead returning a reference.", true,
				Arrays.deepEquals(sudoku.getBoard(),	arr));

		// verify we don't have a shallow copy
		Assert.assertEquals(".getBoard() is not returning a copy, and is instead returning a reference.", false,
				arr == sudoku.getBoard());
	}

	/**
	 * fills row 0 up with 1-9 and verifies it's valid then fills row 0 with all 1's and verifies it isn't valid
	 */
	@Test
	public void isRowValidTest() {
		for (int i = 0; i < sudoku.BOARD_SIZE; i++) {
			sudoku.makeChoice(i+1, 0, 0);
		}
		Assert.assertEquals(".isRowValid() is not returning true when elements 1-9 are inserted in row 0.", true,
				sudoku.isRowValid(0));

		for (int i = 0; i < sudoku.BOARD_SIZE; i++) {
			sudoku.makeChoice(1, 0, i);
		}
		Assert.assertEquals("isRowValid() is not returning false when 1's are inserted repeatedly into row 0", false,
				sudoku.isRowValid(0));
	}

	/**
	 * calls .isRowValid() on an invalid row to make sure a InvalidBoardPositionException gets thrown properly
	 */
	@Test (expected = InvalidBoardPositionException.class)
	public void isRowValidTestIBPE() {
		sudoku.isRowValid(9);
	}

	/**
	 * fills column 0 up with 1-9 and verifies it's valid then fills column 0 with all 1's and verifies it isn't valid
	 */
	@Test
	public void isColumnValidTest() {
		for (int i = 0; i < sudoku.BOARD_SIZE; i++) {
			sudoku.makeChoice(i+1, 0, 0);
		}
		Assert.assertEquals(".isColumnValid() is not returning true when elements 1-9 are inserted in column 0.", true,
				sudoku.isColumnValid(0));

		for (int i = 0; i < 9; i++) {
			sudoku.makeChoice(1, i, 0);
		}
		Assert.assertEquals(".isColumnValid() is not returning false when 1's are inserted repeatedly into column 0", false,
				sudoku.isColumnValid(0));
	}

	/**
	 * calls .isColumnValid() on an invalid row to make sure a InvalidBoardPositionException gets thrown properly
	 */
	@Test (expected = InvalidBoardPositionException.class)
	public void isColumnValidTestIBPE() {
		sudoku.isColumnValid(9);
	}

	/**
	 * fills region 0 up with 1-9 and verifies it's valid then fills region 0 with some 1's and verifies it isn't valid
	 */
	@Test
	public void isRegionValidTest() {
		sudoku.populateBoard(81);

		for (int i = 0; i < sudoku.BOARD_SIZE; i++) {
			Assert.assertEquals(".isRegionValid(" + i + ") does not return true when the region is populated with 1-9.", true,
					sudoku.isRegionValid(i));
		}

		sudoku.populateBoard(0);
		for (int i = 1; i < 3; i++) {
			for (int j = 1; j < 3; j++) {
				sudoku.makeChoice(1, i, j);
			}
		}

		Assert.assertEquals(".isRegionValid() does not return false when the region is populated with repeated 1's", false,
				sudoku.isRegionValid(0));
	}

	/**
	 * calls .isRegionValid() on an invalid row to make sure a InvalidBoardPositionException gets thrown properly
	 */
	@Test (expected = InvalidBoardPositionException.class)
	public void isRegionValidTestIBPE() {
		sudoku.isRegionValid(9);
	}

	/**
	 * creates an empty board to make sure .isBoardFull returns false, then creates a full board to make sure
	 * .isBoardFull returns true
	 */
	@Test
	public void isBoardFullTest() {
		sudoku.populateBoard(80);
		Assert.assertEquals("After .populateBoard(80), .isBoardFull() isn't returning false", false, sudoku.isBoardFull());

		sudoku.populateBoard(81);
		Assert.assertEquals("After .populateBoard(81), .isBoardFull() isn't returning true", true, sudoku.isBoardFull());
	}

	/**
	 * creates 2 boards that aren't solved (using .populateBoard()) and ensures it returns false while using
	 * .populateBoard(81) returns true, as it pulls from solved boards
	 */
	@Test
	public void isSolvedTest() {
		sudoku.populateBoard(0);
		Assert.assertEquals("Board was populated with .populateBoard(0), should've return false", false, sudoku.isSolved());

		sudoku.populateBoard(40);
		Assert.assertEquals("Board was populated with .populateBoard(40), should've return false", false, sudoku.isSolved());

		sudoku.populateBoard(81);
		Assert.assertEquals("Board was populated with .populateBoard(81), should've return true", true, sudoku.isSolved());
	}

	@Override
	public String toString() {
		return "SudokuTest{" +
				"sudoku=" + sudoku +
				'}';
	}
}
