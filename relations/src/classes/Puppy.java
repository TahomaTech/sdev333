package classes;

public class Puppy {
	// fields
	private String name;
	private String nickname;
	private int monthsOld;
	private double weightLbs;

	public Puppy(String name, String nickname, int monthsOld, double weightLbs) {
		this.name = name;
		this.nickname = nickname;
		this.monthsOld = monthsOld;
		this.weightLbs = weightLbs;
	}

	@Override
	public boolean equals(Object other) {
		// make sure we have a puppy | if other is null, or isn't a Puppy, return false
		if (other == null || !(other.getClass().equals(getClass()))) {
			return false;
		}

		// we have a puppy!
		Puppy puppy = (Puppy)other;

		// if their names are equal, they're puppies
		return puppy.name.equals(getName());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public int getMonthsOld() {
		return monthsOld;
	}

	public void setMonthsOld(int monthsOld) {
		this.monthsOld = monthsOld;
	}

	public double getWeightLbs() {
		return weightLbs;
	}

	public void setWeightLbs(double weightLbs) {
		this.weightLbs = weightLbs;
	}

	@Override
	public String toString() {
		return "Puppy{" +
				"name='" + name + '\'' +
				", nickname='" + nickname + '\'' +
				", monthsOld=" + monthsOld +
				", weightLbs=" + weightLbs +
				'}';
	}
}
