package practice;

/**
 * This class represents a triad of three Integers
 * @author Ryan H.
 * @version 1
 */
public class IntegerTriad extends Triad<Integer> {

	/**
	 * Default constructor
	 * @param first - Integer
	 * @param second - Integer
	 * @param third - Integer
	 */
	public IntegerTriad(Integer first, Integer second, Integer third) {
		super(first, second, third);
	}
}
