package practice;

import java.util.List;

public class TestTriads
{
    public static void main(String[] args)
    {
        // make a new Triad of Strings and print them
        StringTriad wordTriplet = new StringTriad("cat", "dog", "cat-dog");
        System.out.println(wordTriplet);
        System.out.println();

        // sort the Triad we made, then print it - THIS SHOWS THAT STRINGS ARE COMPARABLE
        List<String> sorted = wordTriplet.sort();
        for (String s : sorted) {
            System.out.println(s);
        }

        IntegerTriad intTriplet = new IntegerTriad(2, 1, 3);
        System.out.println(intTriplet);
        System.out.println();

        // sort the Triad we made, then print it - THIS SHOWS THAT STRINGS ARE COMPARABLE
        List<Integer> sortedInts = intTriplet.sort();
        for (Integer i : sortedInts) {
            System.out.println(i);
        }
    }
}
