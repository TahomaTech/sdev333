package search;

import java.util.Arrays;

/**
 * Node is a class that supports the DictionarySearch class. Each Node contains their data and then an array of 26
 * chars that point to additional Nodes. This allows us to spell words out via Nodes for our DictionarySearch.
 * @author Ryan Hendrickson
 * @version 1.0
 */
public class Node<T> {
	// fields
	public static final int ALPHABET_SIZE = 26;
	public static final int ASCII = 97;
	private Node<T>[] children;
	private T data;
	private boolean endOfWord;
	private boolean leaf;

	/**
	 * Node constructor. We only take data, and then add the children property to each Node, assume it isn't the end of
	 * a word (and look for this in DictionarySearch) and assume it's a leaf
	 * @param data - data to be contained in the Node
	 */
	public Node(T data) {
		this.data = data;
		this.children = new Node[ALPHABET_SIZE];
		this.endOfWord = false;
		this.leaf = false;
	}

	/**
	 * Node constructor. Accepts data and whether this Node is the end of a word or not
	 * @param data - data to be contained in the node
	 * @param endOfWord - true if this Node is the end of a word, false otherwise
	 */
	public Node(T data, boolean endOfWord) {
		this.data = data;
		this.children = new Node[ALPHABET_SIZE];
		this.endOfWord = endOfWord;
		this.leaf = false;
	}

	/**
	 * acts mostly as a helped method, but also gets called in DictionarySearch to see if a Node's children element
	 * contains that letter already.
	 * @param element - letter to see if it's stored in a node already in children
	 * @return index based off the letter stored in the Node
	 */
	public int getIndex(T element) {
		return (char) element - ASCII;
	}

	/**
	 * @return Node array that either has null values or additional Nodes holding letters
	 */
	public Node<T>[] getChildren() {
		return children;
	}

	/**
	 * @param node - Node that is being added to the children field
	 */
	public void setChildren(Node<T> node) {
		children[getIndex(node.data)] = node;
	}

	/**
	 * @return data - letter that is stored in the Node
	 */
	public T getData() {
		return data;
	}

	/**
	 * @param data - letter to store in the Node
	 */
	public void setData(T data) {
		this.data = data;
	}

	/**
	 * @return endOfWord - boolean to see if that Node is the end of a word
	 */
	public boolean isEndOfWord() {
		return endOfWord;
	}

	/**
	 * @param endOfWord - boolean telling us if the Node is the end of a word or not
	 */
	public void setEndOfWord(boolean endOfWord) {
		this.endOfWord = endOfWord;
	}

	/**
	 * @return true if the Node is a leaf, false otherwise
	 */
	public boolean isLeaf() {
		return leaf;
	}

	/**
	 * @param leaf - tells us if the Node is a leaf
	 */
	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	@Override
	public String toString() {
		return "Node{" +
				"children=" + Arrays.toString(children) +
				", data=" + data +
				", endOfWord=" + endOfWord +
				", leaf=" + leaf +
				'}';
	}
}
