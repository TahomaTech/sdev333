package search;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

/**
 * Stores a dictionary that provides definitions given a word
 * or partial matching for words in the dictionary.
 *
 * @author Ryan Hendrickson
 * @version 1.0
 */
public class DictionarySearch implements IDictionary {
	private Map<String, String> dictionary = new HashMap<>();
	private Tree tree = new Tree();

	/**
	 * Creates a new search object with a dictionary loaded and ready for searching.
	 */
	public DictionarySearch() {
		// try to create a BufferedReader from the file we want to read
		try (BufferedReader br = new BufferedReader(new FileReader("files/dictionary80.txt"))) {
			// prepare line to read in our file
			String line;

			// read file line by line
			while ((line = br.readLine()) != null) {
				// split the line by :
				String[] parts = line.split(":");
				// put word and definition into our dictionary
				dictionary.put(parts[0], parts[1]);
				tree.add(parts[0]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getDefinition(String word) {
		return dictionary.get(word);
	}

	@Override
	public String[] getPartialMatches(String search) {
		return tree.partialMatches(search);
	}

	@Override
	public String toString() {
		return "DictionarySearch{" +
				"tree=" + tree +
				'}';
	}
}
