package comparables.rectangles;

public class Rectangle implements Comparable<Rectangle>
{
	private double width;
	private double height;

	public Rectangle(double width, double height)
	{
		this.width = width;
		this.height = height;
	}

	public double getWidth()
	{
		return width;
	}

	public double getHeight()
	{
		return height;
	}

	public double area() {
		return width * height;
	}

	public String toString()
	{
		return "w: " + getWidth() + ", h: " + getHeight() + ", area: " + area();
	}

	@Override
	public int compareTo(Rectangle rec) {
		double thisArea = area();
		double otherArea = rec.area();

		return Double.compare(thisArea, otherArea);
	}
}

