package comparators.practice.albums;

import java.util.Comparator;

public class Comparators {
	//declare nested Comparator classes here...

	//NOTE: You should make the classes static to make
	//it easier to instantiate each class elsewhere

	public static class TitleSorter implements Comparator<Album> {
		@Override
		public int compare(Album o1, Album o2) {
			return o1.getTitle().compareTo(o2.getTitle());
		}
	}

	public static class YearSorter implements Comparator<Album> {
		@Override
		public int compare(Album o1, Album o2) {
			return Integer.compare(o2.getYearReleased(), o1.getYearReleased());
		}
	}

	public static class MinutesSorter implements Comparator<Album> {
		@Override
		public int compare(Album o1, Album o2) {
			return Double.compare(o1.getMinutes(), o2.getMinutes());
		}
	}

	public static class CompletedSorter implements Comparator<Album> {
		@Override
		public int compare(Album o1, Album o2) {
			return Boolean.compare(o1.isCompleted(), o2.isCompleted());
		}
	}
}
