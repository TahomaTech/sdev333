package rgb;

import java.util.Iterator;

/**
 * @author Ryan H.
 * @version 1.0
 * tests our RGBColor.java
 */
public class RGBDriver {

	/**
	 * @param args - we all know what args is
	 */
	public static void main(String[] args) {
		final int red = 155;
		final int green = 100;
		final int blue = 240;
		RGBColor rgb = new RGBColor(red, green, blue);

		// using an Iterator directly
		Iterator<Integer> cols = rgb.iterator();
		while (cols.hasNext()) {
			System.out.println(cols.next());
		}
	}
}
