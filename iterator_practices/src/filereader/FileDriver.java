package filereader;

/**
 * @author Ryan H.
 * @version 1.0
 * tests our FileReader.java
 */
public class FileDriver {
	/**
	 * @param args - arguments for String[]
	 */
	public static void main(String[] args) {

		int count = 0;
		for (String line : new FileReader("war-and-peace.txt")) {
			System.out.println(line);
			count++;
		}
		System.out.println("Lines in file: " + count);
	}
}
